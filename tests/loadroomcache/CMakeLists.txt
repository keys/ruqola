# SPDX-FileCopyrightText: 2020-2022 Laurent Montel <montel@kde.org>
# SPDX-License-Identifier: BSD-3-Clause
add_executable(loadroomcachetest loadroomcache.cpp loadroomcache.h)

target_link_libraries(loadroomcachetest
    Qt::Widgets
    KF5::KIOWidgets
)
