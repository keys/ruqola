/*
   SPDX-FileCopyrightText: 2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include <KConfigGroup>

namespace KPIMTextEditTextToSpeech
{
namespace TextToSpeechUtil
{
Q_REQUIRED_RESULT QString textToSpeechConfigFileName();
Q_REQUIRED_RESULT QString textToSpeechConfigGroupName();
};
}
